import os
import datetime
import shutil
import sys

from PyQt5 import QtWidgets, uic, QtCore, QtGui


from connect_SQLite import Connection, SQLiteConnectionError

class IdUi(QtWidgets.QDialog):

    def __init__(self): #TODO: overwrite done() to verify if id exist
        super(IdUi, self).__init__()
        uic.loadUi('GUI/id.ui', self)
        validator = QtGui.QIntValidator
        validator.setBottom(0)
        self.textId.setValidator(validator)

class Menu(QtWidgets.QDialog):

    def __init__(self, mode):
        super(Menu, self).__init__()
        self.db_name = 'meals.db'
        self.db_path = self.db_name

        if mode == 1:
            self.loadUiAdd()
        elif mode ==2:
            self.loadUiModify()
    
    def loadUiAdd(self):
        uic.loadUi('GUI/addMenu.ui', self)
        validator = QtGui.QDoubleValidator()
        validator.setBottom(0)
        validator.setDecimals(2)
        self.text_price.setValidator(validator)
        self.buttonBox.accepted.connect(self.add_menu)
        
    def loadUiModify(self):
        idUi = IdUi()
        #MidUi.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if idUi.exec_() == QtWidgets.QDialog.Accepted:
            id = idUi.textId.text()

            uic.loadUi('GUI/addMenu.ui', self)
            self.setWindowTitle('Modify User')

            sql_query = "SELECT description,price FROM menu WHERE id=?"
            sql_values = (id,)
            with Connection(self.db_path) as db:
                query_result = db.read_from_cursor(sql_query, sql_values) 
            
            self.text_descirption.setText(query_result[0][0])
            self.text_price.setText(query_result[0][1])

            self.buttonBox.accepted.connect(self.modify_menu)

   
    def add_menu(self):
        
        description = self.text_description.text()
        price = self.text_price.text()

        sql_query = "INSERT INTO menu(description,price) VALUES (?,?)"
        sql_values = (description, price)
        with Connection(self.db_path) as db:
            db.write_to_cursor(sql_query, sql_values)
            db.commit_to_db()
    
    def modify_menu(self):
        with Connection(self.db_path) as db:
            sql_query = "UPDATE menu SET price = ?, description = ? WHERE id=?"
            sql_values = (self.text_price.text(), self.text_description.text(), menu_id)
            db.write_to_cursor(sql_query, sql_values)

            db.commit_to_db()
    

class User(QtWidgets.QDialog):

    def __init__(self, mode):
        super(User, self).__init__() # Call the inherited classes __init__ method
        self.db_name = 'meals.db'
        self.db_path = self.db_name

        if mode == 1:
            self.loadUiAdd()
        elif mode ==2:
            self.loadUiModify()
    
    def loadUiAdd(self):
        uic.loadUi('GUI/addUser.ui', self)
        self.buttonBox.accepted.connect(self.add_employee)
        
    def loadUiModify(self):
        idUi = IdUi()
        #MidUi.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if idUi.exec_() == QtWidgets.QDialog.Accepted:
            self.id = idUi.textId.text()
            uic.loadUi('GUI/addUser.ui', self)
            self.setWindowTitle('Modify User')

            sql_query = "SELECT first_name,family_name,email_address FROM employee WHERE id=?"
            sql_values = (self.id,)
            with Connection(self.db_path) as db:
                query_result = db.read_from_cursor(sql_query, sql_values) 
            
            self.textEdit_firstName.setText(query_result[0][0])
            self.textEdit_familyName.setText(query_result[0][1])
            self.textEdit_email.setText(query_result[0][2])

            self.buttonBox.accepted.connect(self.modify_employee)



    def add_employee(self):
        self.first_name = self.textEdit_firstName.text()
        self.family_name = self.textEdit_familyName.text()
        self.email_address = self.textEdit_email.text()

        sql_query = "INSERT INTO employee(first_name,family_name,email_address) VALUES (?,?,?)"
        sql_values = (self.first_name, self.family_name, self.email_address)
        with Connection(self.db_name) as db:
            db.write_to_cursor(sql_query, sql_values)
            db.commit_to_db()
            db.save_database(self.db_name)


    def modify_employee(self):
        employee_id = self.id
     
        with Connection(self.db_path) as db:
                sql_query = "UPDATE employee SET first_name = ?, family_name = ?, email_address = ? WHERE id=?"
                sql_values = (self.textEdit_firstName.text(),self.textEdit_familyName.text(), self.textEdit_email.text() , employee_id)
                db.write_to_cursor(sql_query, sql_values)

                db.commit_to_db()
    
class Management():

    def __init__(self):
        self.db_name = 'meals.db'
        self.db_path = self.db_name  # will be different if "db" is not in the same directory

    def purchase(self):
        purchase_id = self.get_purchase_id()
        current_date = datetime.datetime.now()
        formated_date = current_date.strftime("%d-%m-%Y %H:%M:%S")

        employee_id = self.get_employee_id()
        employee_name = self.get_employee_name(employee_id)
        ticket_data = [purchase_id, formated_date, employee_name]

        with Connection(self.db_path) as db:
            sql_query = "INSERT INTO purchase(date,employee_id) VALUES (?,?)"
            sql_values = (formated_date, employee_id)
            db.write_to_cursor(sql_query, sql_values)

        menu_id_list = self.get_id_list('menu')

        while True:
            menu_id = input("\nEnter menu ID : ")

            if not menu_id:  # TODO if menu_id in menu_id_list:
                break

            os.system('cls')
            menu_id = int(menu_id)
            if menu_id in menu_id_list:
                # TODO get_purchase_detail plutôt que get_menu_price?
                menu_price = self.get_menu_price(menu_id)
                sql_query = "INSERT INTO purchase_detail(purchase_id,menu_id,menu_price) VALUES (?,?,?)"
                sql_values = (purchase_id, menu_id, menu_price)
                with Connection(self.db_path) as db:
                    db.write_to_cursor(sql_query, sql_values)
                ticket_data.append(sql_values)
            self.display_ticket(ticket_data)

        confirm = input("Confirm your order (y/n) : ")
        if ((confirm == 'y') and (len(ticket_data) >= 4)):
            db.commit_to_db()
            db.save_database(self.db_name)

    def get_purchase_id(self):
        with Connection(self.db_path) as db:
            sql_query = "SELECT MAX(id) FROM purchase"
            query_result = db.read_from_cursor(sql_query)

        if query_result[0][0] != None:
            last_purchase_id = query_result[0][0]
        else:
            last_purchase_id = 0
        purchase_id = last_purchase_id + 1
        return purchase_id

    def get_employee_id(self):  # TODO idem que get_menu_id()?
        employee_id_list = self.get_id_list('employee')
        employee_id = 0
        while employee_id not in employee_id_list:
            os.system('cls')
            employee_id = input("Enter the employee ID : ")
            if not employee_id:
                employee_id = 0
            else:
                employee_id = int(employee_id)
        return employee_id

    def get_id_list(self, table_name):
        if (table_name == 'employee'):  # TODO simlify with ?
            sql_query = "SELECT id FROM employee"
        elif (table_name == 'menu'):
            sql_query = "SELECT id FROM menu"

        with Connection(self.db_path) as db:
            query_result = db.read_from_cursor(sql_query)
        id_list = []

        for id in query_result:
            id_list.append(id[0])

        return id_list

    def get_employee_name(self, employee_id):
        sql_query = "SELECT first_name,family_name FROM employee WHERE id=?"
        sql_values = (employee_id,)
        with Connection(self.db_path) as db:
            query_result = db.read_from_cursor(sql_query, sql_values)
        employee_name = query_result[0][0] + ' ' + query_result[0][1]
        return employee_name

    def get_menu_price(self, menu_id):
        sql_query = "SELECT price FROM menu WHERE id=?"
        sql_values = (menu_id,)
        with Connection(self.db_path) as db:
            query_result = db.read_from_cursor(sql_query, sql_values)
        menu_price = query_result[0][0]
        return menu_price

    def display_ticket(self, ticket_data):
        purchase_line = 'Purchase number : ' + str(ticket_data[0])
        date_line = 'Date : ' + ticket_data[1]
        employee_line = 'Employee : ' + ticket_data[2] + '\n'

        print(purchase_line)
        print(date_line)
        print(employee_line)

        amount = 0.0
        # [purchase_id,date,employee,(p_id,menu_id,price)]
        for index in range(3, len(ticket_data)):
            menu_id = ticket_data[index][1]
            description = self.get_menu_description(menu_id)
            price = ticket_data[index][2]
            detail_string = "Menu: {:<18} {:>6} €"
            detail_line = detail_string.format(description, price)
            print(detail_line)
            amount = amount + price  # TODO pourquoi 3.3 * 3 donne 9.89999999?
        amount_line = '\nAmount : ' + str(amount) + ' €'
        print(amount_line)

    def get_menu_description(self, menu_id):
        sql_query = "SELECT description FROM menu WHERE id=?"
        sql_values = (menu_id,)
        with Connection(self.db_path) as db:
            query_result = db.read_from_cursor(sql_query, sql_values)
        description = query_result[0][0]
        return description




