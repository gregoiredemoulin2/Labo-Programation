from library import Management, User, Menu
import sys 
from PyQt5 import QtWidgets, uic, QtCore

class Ui(QtWidgets.QMainWindow):
    function_modes= {
        'ADD' : 1,
        'MODIFY' : 2
    }

    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('GUI/main.ui', self) # Load the .ui file
        self.show() # Show the GUI
        self.button_add_user.clicked.connect(self.on_click_add_user)
        self.button_add_menu.clicked.connect(self.on_click_add_menu)
        self.button_modify_user.clicked.connect(self.on_click_modify_user)
        #self.button_modify_menu.clicked.connect(self.on_click_modify_menu)
        self.management = Management()

    def on_click_add_user(self):
        newUserUI = User(self.function_modes['ADD'])
        newUserUI.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if newUserUI.exec_() == QtWidgets.QDialog.Accepted:
            self.screen.setText('ADD USER')
    
    
    def on_click_modify_user(self):
        newUserUI = User(self.function_modes['MODIFY'])
        newUserUI.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if newUserUI.exec_() == QtWidgets.QDialog.Accepted:
            self.screen.setText('MODIFY USER')
    
    
    def on_click_add_menu(self):
        newMenuUI = Menu(self.function_modes['ADD'])
        newMenuUI.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if newMenuUI.exec_() == QtWidgets.QDialog.Accepted:
            self.screen.setText('ADD MENU')
    
    
    def on_click_modify_menu(self):
        newMenuUI = Menu(self.function_modes['MODIFY'])
        newMenuUI.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if newMenuUI.exec_() == QtWidgets.QDialog.Accepted:
            self.screen.setText('MODIFY')
    


if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)

    window = Ui()
    window.show()
    app.exec()
